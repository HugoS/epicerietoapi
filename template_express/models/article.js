module.exports = {
  fetchAll() {
    return connection.execute('SELECT * FROM articles');
  },
  fetchOne(ID) {
    return connection.execute('SELECT * FROM articles where articleId = ?', [
      ID
    ]);
  },
  delete(ID) {
    return connection.execute('DELETE FROM articles WHERE articleId= ?', [ID]);
  },
  add(req) {
    return connection.execute(
      'INSERT INTO articles (name, price) VALUES (?,?)',
      req
    );
  },
  modify(id, body) {
    return connection.execute(
      'UPDATE articles SET name = ?, price = ? WHERE  articleId = ? ',
      [body.name, body.price, id]
    );
  }
};
