const express = require('express');
const router = express.Router();
const models = require('./../models');

router.get('/', async function(req, res) {
  const [rows, fields] = await models.article.fetchAll();
  res.json(rows);
});
router.get('/listArticles', async function(req, res) {
  const [rows, fields] = await models.article.fetchAll();
  res.format({
    'text/html': () => {
      res.render('articles/listArticles', {
        articles: rows
      });
    }
  });
});
router.get('/add', async function(req, res) {
  res.render('articles/add.pug');
});

router.get('/:articleId', async function(req, res) {
  const [rows, fields] = await models.article.fetchOne(req.params.articleId);
  res.json(rows);
});

router.get('/:articleId/modify', async function(req, res) {
  const [rows, fields] = await models.article.fetchOne(req.params.articleId);
  res.format({
    'text/html': () => {
      res.render('articles/modify', {
        articles: rows[0]
      });
    }
  });
});

router.post('/', async function(req, res) {
  try {
    await models.article.add([req.body.name, req.body.price]);
    res.format({
      'text/html': function() {
        res.redirect('/articles/listArticles');
      },
      'application/json': function() {
        res.json('article added with success');
      }
    });
  } catch (err) {
    console.log(err);
  }
});

router.delete('/:articleId', async function(req, res, next) {
  const [rows, fields] = await models.article.delete(req.params.articleId);
  res.format({
    'text/html': () => {
      res.redirect('/articles/listArticles');
    }
  });
});

router.post('/:artcleId', async function(req, res) {
  try {
    const [rows, fields] = await models.artilce.modify(
      req.params.articleId,
      req.body
    );
    console.log('modifyed with success');
    res.format({
      'text/html': function() {
        res.redirect('/articles/listArticles');
      },
      'application/json': function() {
        res.json(rows);
      }
    });
  } catch (err) {
    console.log(err);
  }
});

router.put('/:articleId', async function(req, res) {
  const [rows, fields] = await models.article.modify(
    req.params.articleId,
    req.body
  );
  res.redirect('listArticles');
});

module.exports = router;
