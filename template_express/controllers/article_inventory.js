const express = require('express');
const router = express.Router();
const models = require('./../models');

router.get('/', async function(req, res) {
  const [rows, fields] = await models.article_inventory.fetchAll();
  res.send(rows);
});

router.get('/listInventory', async function(req, res) {
  const [rows, fields] = await models.article_inventory.fetchAll();
  res.format({
    'text/html': () => {
      res.render('articles_inventory/listInventory', {
        inventorys: rows
      });
    }
  });
});

router.get('/addToInventory', async function(req, res) {
  res.render('articles_inventory/addToInventory.pug');
});

router.delete('/:articleId', async function(req, res) {
  console.log(req.params.articleId);
  await models.article_inventory.delete(req.params.articleId);
  res.format({
    'text/html': () => {
      res.redirect('/articles_inventory/listInventory');
    }
  });
});

router.post('/', async function(req, res) {
  try {
    console.log(req.body.inventoryId);
    const [rows, fields] = await models.article_inventory.add([
      req.body.quantity,
      req.body.articleId,
      req.body.inventoryId
    ]);
    res.format({
      'text/html': function() {
        res.redirect('/articles_inventory/listInventory');
      },
      'application/json': function() {
        res.json(rows);
      }
    });
  } catch (err) {
    console.log(err);
  }
});

router.post('/:artcleId', async function(req, res) {
  try {
    const [rows, fields] = await models.artilce.modify(
      req.params.articleId,
      req.body
    );
    console.log('modifyed with success');
    res.format({
      'text/html': function() {
        res.redirect('/articles/listArticles');
      },
      'application/json': function() {
        res.json(rows);
      }
    });
  } catch (err) {
    console.log(err);
  }
});

router.put('/:articleId', async function(req, res) {
  const [rows, fields] = await models.article.modify(
    req.params.articleId,
    req.body.name
  );
  res.json(rows);
});

module.exports = router;
